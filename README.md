#安装方式
- 呃，仅提供git+vundle安装方式

## 1.获得.vimrc
- git clone https://git.oschina.net/iamdsy/iamdsy_vimrc.git
- cp -rv ./iamdsy_vimrc/plugin/\_vimrc_include_vundle ~/.vimrc

## 2.安装vundle
- git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

## 3.安装.vimrc中的所有插件，然后就可以使用了
- 打开vim，在vim中输入:PluginInstall就可以了

