" 判断操作系统类型
if(has('win32') || has('win64'))
	let g:isWIN = 1
	let g:isMAC = 0
else
	if system('uname') =~ 'Darwin'
		let g:isWIN = 0
		let g:isMAC = 1
	else
		let g:isWIN = 0
		let g:isMAC = 0
	endif
endif

" 判断是否处于GUI界面
if has('gui_running')
	let g:isGUI = 1
else
	let g:isGUI = 0
endif

" 使用GUI界面时的设置
if g:isGUI
	if g:isWIN
		au GUIEnter * simalt ~x  " 启动时自动最大化窗口
	else
		set lines=999 columns=999
	endif
	set guioptions+=c        " 使用字符提示框
	set guioptions-=m        " 隐藏菜单栏
	set guioptions-=T        " 隐藏工具栏
	set guioptions-=L        " 隐藏左侧滚动条
	set guioptions-=r        " 隐藏右侧滚动条
	set guioptions-=b        " 隐藏底部滚动条
	set showtabline=0        " 隐藏Tab栏
	set cursorline           " 高亮突出当前行
	colo Tomorrow-Night-Blue "设置配色方案
else
	colo Tomorrow-Night-Blue
	set background=light
endif


" 设置文件编码和文件格式
set encoding=utf-8 "文件编码
set fileencodings=ucs-bom,utf-8,cp936,gb18030,big5,euc-jp,euc-kr,latin1 "自动识别编码
set fileformats=unix,mac,dos "自动识别文件格式？
if g:isWIN
	source $VIMRUNTIME/delmenu.vim
	source $VIMRUNTIME/menu.vim
	language messages zh_CN.utf-8
endif

set relativenumber	"显示相对行号
set number              "显示行号"
set hls			"搜索时高亮显示找到的文本
set is			"搜索时在未完全输入完毕要检索的文本时就开始检索
set backspace=2		"设置退格键可以作为删除键使用
set hidden		"允许在有未保存的修改时切换缓冲区
set autochdir		"设定浏览器目录为当前目录
set nobackup		"不生成备份文件
set noswapfile		"不生成交换文件
set ignorecase          "查询时忽略大小写
set scrollbind		"让分屏的几个同步滚屏
set laststatus=2        "总是显示状态栏
set statusline+=%F       "状态栏显示文件名称及路径
set statusline+=\ \     "分隔符
set statusline+=%m       "用+号表示文件已被修改
set statusline+=\ \     "分隔符
set statusline+=%{strftime(\"%Y-%m-%d\ %X\")} "显示时间，但必须要光标动时间才有变化，这对强迫症来说也是很蛋疼的事啊  
set statusline+=%=        "将后面的显示到状态栏的右边
set statusline+=%l       "右边显示文件行号
set statusline+=/      "分隔符
set statusline+=%L       "右边显示文件总行数
set statusline+=\ \ \ \ "空格标签
set statusline+=%c      "列号
set statusline+=\ \ \ \ "空格标签
set statusline+=%p      "百分比
set statusline+=%%      "百分号？
set statusline+=\ \ \ \  "空格标签
"set ru			"显示vim的状态栏标示，如行号、列号、百分比等,设置了状态栏的样式后，这条似乎就无效了
"设置K关键字为:help
set keywordprg=:help
"设置字体及大小
if isGUI
	if isWIN
		set guifont=Monospac821_BT:h12:cANSI:qDRAFT
	else
		set guifont=DejaVu\ Sans\ Mono\ 12
	endif
endif
"set foldmethod=indent   "这样只要有缩进，就可以用zc、zC、zo、ZO进行折叠相关操作"


syntax on		"打开关键字上色，如if else,注释等

"为了让各种插件起作用做的各种配置
"vim-lua-ftplugin
"配置之前需要安装lua.exe、luac.exe
"此配置能实现语法检测(需要luac)、标准库补全、LUA_PATH路径的模块名称补全
"不支持自写模块的变量名称补全，不知道是没配置好，还是本身就不支持
"支持c-x c-u,c-x c-o快捷键
if g:isWIN 
	:let g:lua_interpreter_path = 'd:\tools\openresty\resty'
	:let g:lua_compiler_name = 'd:\tools\Lua\5.3\luac.exe'
	"	:let g:lua_interpreter_path = 'd:\tools\openresty\luajit.exe'
	"	:let g:lua_compiler_name = 'd:\tools\openresty\luajit.exe'
	:let $LUA_PATH = 'd:\tools\openresty\lualib\resty\?.lua'
	:let $LUA_CPATH = 'd:\tools\openresty\lualib\?.so'
	:let g:lua_complete_omni = 1 "支持c-x c-o快捷键
	"	set tags=d:\tools\openresty\lualib\tags
else
	:let g:lua_interpreter_path = '/usr/local/openresty/bin/resty'
	:let $LUA_PATH = '/usr/local/openresty/lualib/resty/?.lua'
	":let $LUA_PATH = '/home/iamdsy/quickx/framework/?.lua'
	:let $LUA_CPATH = '/usr/local/openresty/lualib/?.so'
	:let g:lua_complete_omni = 1 "支持c-x c-o快捷键
	set completeopt=longest,menu
	"	set tags=/usr/local/openresty/lualib/tags
endif

"配置让youcompleteme起作用
if isWIN 
	if filereadable('$HOME/ycm_build/ycm/Release/ycm_core.lib')
		:let g:ycm_global_ycm_extra_conf = '$HOME/.ycm_extra_conf_py'
	else 
		:let g:ycm_global_ycm_extra_conf = '$HOME/.vim/bundle/YouCompleteMe/third_party/ycmd/cpp/ycm/.ycm_extra_conf.py'
	endif
endif

"自定义快捷键：
" ---------- Ctrl系按键 ----------
"
" Ctrl + H                   --光标移当前行不是blank字符行首       [插入模式]
:inoremap <c-h> <esc>^i
:noremap <c-h> <c-w>h
" Ctrl + J                   --光标移下一行行首       [插入模式]
:inoremap <c-j> <esc>j<esc>^i
:noremap <c-j> <c-w>j
" Ctrl + K                   --光标移上一行行尾       [插入模式]
:inoremap <c-k> <esc>k<esc>$a
:noremap <c-k> <c-w>k
" Ctrl + L                   --光标移当前行行尾       [插入模式]
:inoremap <c-l> <esc>$a
:noremap <c-l> <c-w>l
" ctrl + d                   --删除光标所在位置的字符 [插入模式]
:inoremap <c-d> <esc>lxi
" ctrl + b                   --往回删除一个字符       [插入模式]
:inoremap <c-b> <esc>xi
" ---------- Meta系按键 ----------
" Alt  + H                   --光标左移一格           [插入模式] [Linux下可能失效]
" Alt  + J                   --光标下移一格           [插入模式] [Linux下可能失效]
" Alt  + K                   --光标上移一格           [插入模式] [Linux下可能失效]
" Alt  + L                   --光标右移一格           [插入模式] [Linux下可能失效]

if g:isWIN
	:inoremap <m-h> <left>
	:inoremap <m-j> <down>
	:inoremap <m-k> <up>
	:inoremap <m-l> <right>
else
	:inoremap <esc>h <left>
	:inoremap <esc>j <down>
	:inoremap <esc>k <up>
	:inoremap <esc>l <right>
endif


"---------<c-x>系列快捷键-------------------------------
"snipmate的触发按键

:imap <c-x><c-x>         <Plug>snipMateNextOrTrigger
" ---------- Leader系按键 ----------
"

",nb 切换下一个buffer
",np 切换上一个buffer
let mapleader = ","
:nnoremap <Leader>nb :bnext<cr>
:nnoremap <Leader>np :bprevious<cr>
"git能使用的前提条件
"windows下面需要安装带ssh的git，并将git所在加入path
"git的remote要添加为git格式的
"git的id_rsa要放到$HOME\.ssh目录下面
"让vim的当前目录调整到当前文件所在的目录
"set autochdir好像就可以了，相当于下面的一堆lcd等等功能
"lcd只针对当前文件，cd设置全局目录
":lcd %:p:h,此命令只能在git之前单独输入
",d  调整目录为当前文件目录
":nnoremap <Leader>d :lcd %:p:h <cr>

",add 将所有文件加到git中
:nnoremap <Leader>add :!git add ./* <cr><cr>

",add 将相关文件加入git
:nnoremap <Leader>add :!git add ./* <cr><cr>

",m 提交git commit
:nnoremap <Leader>m  :!git commit -m "test" -a <cr><cr>

",gp 让git push到git.oschina.net
:nnoremap <Leader>gp :!git push origin master <cr><cr>

",i 安装plugin
:nnoremap <Leader>i :PluginInstall <cr>

",y                         --复制至公共剪贴板       [仅选择模式]
:vnoremap <leader>y "+y
",a                         --复制所有至公共剪贴板   [Normal模式可用]
:nnoremap <leader>a ggvG"+y
",p                         --从公共剪贴板粘贴       [全模式可用]
:noremap <leader>p "+p
:noremap! <leader>p <esc>"+pi
",rb                        --一键去除所有尾部空白   [全模式可用]
inoremap <leader>rb <esc>:let _s=@/<bar>:%s/\s\+$//e<bar>:let @/=_s<bar>:nohl<cr>
nnoremap <leader>rb :let _s=@/<bar>:%s/\s\+$//e<bar>:let @/=_s<bar>:nohl<cr>
vnoremap <leader>rb <esc>:let _s=@/<bar>:%s/\s\+$//e<bar>:let @/=_s<bar>:nohl<cr>
",rm                        --一键去除^M字符         [全模式可用]
inoremap <leader>rm <esc>:%s/<c-v><c-m>//g<cr>
nnoremap <leader>rm :%s/<c-v><c-m>//g<cr>
vnoremap <leader>rm <esc>:%s/<c-v><c-m>//g<cr>
",w 			  --保存buffer		     [全模式可用]
:noremap! <leader>w <esc>:w<cr>
:noremap <leader>w :w<cr> 
",q                        --退出vim
:noremap <leader>q :q<cr>
:noremap! <leadr>q <esc>:q<cr>
",o                       --取消分屏
:noremap <leader>o :only<cr>
:noremap! <leader>o <esc>:only<cr>
",nt                        --打开/关闭NERDTree文件树窗口
:noremap <leader>nt :NERDTreeToggle<cr>
:noremap! <leader>nt <esc>:NERDTreeToggle<cr>
",sb                         --设置scrollbind，同步滚屏
:noremap <leader>sb :set scrollbind<cr>
:noremap! <leader>sb <esc>:set scrollbind<cr>
",nsb                        --设置noscrollbind，取消同步滚屏
:noremap <leader>nsb :set noscrollbind<cr>
:noremap! <leader>nsb <esc>:set noscrollbind<cr>
",ev                        --编辑当前所使用的Vim配置文件
if g:isWIN
	:noremap <leader>ev  :e $HOME\_vimrc<cr>
else
	:noremap <leader>ev :e ~/.vimrc<cr>
endif
",s                        --source %
:noremap <leader>s :source %<cr>
:noremap! <leader>s <esc>:source %<cr>
",bw                       --关闭当前buffer
:noremap <leader>bw :bw<cr>
let localmapleader = ","
",nhl                     --取消搜索某字符的高亮显示
:noremap <leader>nhl :nohlsearch<cr>
:noremap! <leader>nhl <esc>:nohlsearch<cr>
",t                       --绑定有道的vim插件，翻译光标所在单词
:noremap <leader>t :<c-u>Ydc<cr>
:vnoremap <leader>t :<C-u>Ydv<CR>
",yd                      --在命令行输入单词，给出翻译
:noremap <leader>yd :<C-u>Yde<CR>
"为了在vim下面看英文文档，安装了w3m-vim和w3m.exe,设置了相关设置
:noremap <leader>w3m :W3mTab
",c                        --注释当前文件
"针对vim文件,目前仅对当前行有效
:au FileType vim nnoremap <buffer> <leader>c I"<esc>
:au FileType vim nnoremap <buffer> <leader>uc 0x

"针对lua文件,目前仅对当前行有效
:au FileType lua nnoremap <buffer> <leader>c I--<esc>
:au FileType lua nnoremap <buffer> <leader>uc 0xx

",tag打开tagbar
:noremap <leader>tag :TagbarOpen<cr>

"tab映射为自动缩进
:nnoremap <tab> ==
":inoremap <tab> <esc>==
:vnoremap <tab> =
"映射jk为esc
:inoremap jk <esc>
":inoremap <esc> <nop>
"自动复制_vimrc文件为~/.vim/bundle/iamdsy_vimrc/plugin/_vimrc_include_vundle
if isWIN 
	:au BufWritePost _vimrc :execute  ":!cp " . expand("$HOME") . '\_vimrc ' . expand("$HOME") . '\.vim\bundle\iamdsy_vimrc\plugin\_vimrc_include_vundle'
else
	:au BufWritePost .vimrc :!cp ~/.vimrc ~/.vim/bundle/iamdsy_vimrc/plugin/_vimrc_include_vundle
endif
"设置ycm的自动补全的触发键为c-]
":let g:ycm_key_invoke_completion = '<c-]>'
"打开ycm自动补全时候的preview窗口，补全完成后就自动关闭该窗口，此窗口能显示函数原型
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_add_preview_to_completeopt = 1
"为了使下面的enter能起作用，需要注释auto-pairs.vim的下面这一行
"execute 'inoremap <script> <buffer> <silent> <CR> '.old_cr.'<SID>AutoPairsReturn'

"let g:ycm_key_list_select_completion = ['<TAB>', '<Down>', '<Enter>']
" 
"让自动补全的提示菜单好看些
highlight Pmenu    guibg=darkgrey  guifg=black ctermbg=5  ctermfg=white
highlight PmenuSel guibg=lightgrey guifg=black ctermbg=4  ctermfg=white

"为了使用vim编辑markdown的时候在右侧能看到目录，
"需要在vimrc中安装'xolox/vim-easytags'、'majutsushi/tagbar'、'jszakmeister/markdown2ctags'
"然后再增加如下几句到.vimrc中就可以了
let g:tagbar_type_markdown = {
    \ 'ctagstype': 'markdown',
    \ 'ctagsbin' : '~/.vim/bundle/markdown2ctags/markdown2ctags.py',
    \ 'ctagsargs' : '-f - --sort=yes',
    \ 'kinds' : [
        \ 's:sections',
        \ 'i:images'
    \ ],
    \ 'sro' : '|',
    \ 'kind2scope' : {
        \ 's' : 'section',
    \ },
    \ 'sort': 0,
\ }
"设置tagbar打开后选择tag调到对应的tag后关闭tagbar
let g:tagbar_autoclose = 1
"设置vim内的各种缩进
autocmd FileType php,python,c,java,perl,shell,bash,vim,ruby,cpp,lua set ai
autocmd FileType php,python,c,java,perl,shell,bash,vim,ruby,cpp,lua set sw=2
autocmd FileType php,python,c,java,perl,shell,bash,vim,ruby,cpp,lua set ts=2
autocmd FileType php,python,c,java,perl,shell,bash,vim,ruby,cpp,lua set sts=2
autocmd FileType javascript,html,css,xml set ai
autocmd FileType javascript,html,css,xml set sw=2
autocmd FileType javascript,html,css,xml set ts=2
autocmd FileType javascript,html,css,xml set sts=2
"让tab缩进的各种文件能显示垂直对齐竖线
:set list lcs=tab:\|\  "注意第二个反斜杠后有一个空格，好扯淡 
"让lua类型文件不要用ycm
let g:ycm_filetype_blacklist = {
			\ 'lua' : 1
			\}
"让代码按缩进进行折叠
set fdm=indent
"让autocomplepop的选项可以用tab来选择
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<C-g>u\<Tab>"
"设置ycm的python路径
"g:ycm_server_python_interpreter' option to a Python 2 interpreter path
let g:ycm_server_python_interpreter = '/usr/bin/python2.7'
:nnoremap <Leader>td :call InsertDate()<cr>
:nnoremap <Leader>do :call Done()<cr>



